package concurrent;

public class WorkerThread implements Stoppable{
    private final Object taskPresentMonitor = new Object();
    private volatile Stoppable curTask;
    private final Thread t;
    private final int timeout;
    private volatile boolean isRunning;
    private final ThreadPool tP;
    
    public WorkerThread(ThreadPool tp, int tout){
            tP = tp;
            timeout = tout;
            t = new Thread(this); 
            t.start();
    }

    @Override
    public void run() {
        String session = "";

        while(curTask == null) {
            synchronized (taskPresentMonitor) {
                try {
                    taskPresentMonitor.wait(timeout);
                } catch (InterruptedException e) {
                    tP.hEnd(this, session, "Timeout");
                }
            }
        }
        try{
            session = curTask.toString();
            tP.hStart(this, session);
            curTask.run();
            curTask = null;
        }finally{
            tP.hEnd(this, session, "Finished");
        }
    }

    @Override
    public void stop(){
        if(isRunning){
            isRunning = false;
            if(curTask != null)
                curTask.stop(); 
            if(t != null)
                t.interrupt();  
        }
    }

    public void stopWork(){
        if(curTask != null)
            curTask.stop();   
    }

    public void setTask(Stoppable r){
        synchronized(taskPresentMonitor){
            if(curTask != null)
                throw new IllegalStateException();
            curTask = r;
            taskPresentMonitor.notify();
        }
    }

}
