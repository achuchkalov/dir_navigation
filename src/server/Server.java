package server;

import cmd.CmdHandler;
import concurrent.Dispatcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import net.*;

public class Server {
    private Host h;
    private Dispatcher d;
    private boolean isRunning;
    private final CallbackHandler callbackHandler;
    private final int maxClientsCount = 2;

    public Server(CallbackHandler c) {
        callbackHandler = c;
        isRunning = false;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        final Boolean[] finished = {false};

        String cmd;

        Server server = new Server(new CallbackHandler() {
            @Override
            public void handle(String state) {
                if (state.equalsIgnoreCase("Connected")) {
                    System.out.println("Session started");
                } else {
                    System.out.println("Session ended");
                }
            }
        });
        try {
            server.start(Integer.parseInt(args[0]));
        } catch (NullPointerException | NumberFormatException | IllegalStateException e) {
            System.out.println("Failed to start server");
            return;
        }

        // обработка внешнего прерывания работы
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                System.out.println("Process was interrupted, stopping server.");
                server.stop();
            }
        });

        System.out.println("Type 'exit' to stop server");
        cmd = in.readLine();
        while (!cmd.equals("exit")) {
            cmd = in.readLine();
        }
        if (cmd.equals("exit"))
            server.stop();
    }

    public boolean start(int port){
        if(!isRunning){
            try {
                Channel<Session> ch = new Channel();
                h = new Host(port, ch, CmdHandler.class);
                d = new Dispatcher(ch, 1, maxClientsCount, 120, callbackHandler);
                h.initStart();
                d.initStart();
                isRunning = true;
                System.out.println("Server: started");
                return true;
            } catch (IOException e) {
                this.stop();
                System.out.println("Error: Something crashed...");
                return false;
            }
        } else throw new IllegalStateException("Error: Already started");
    }

    public void stop()
    {
        if(isRunning){
            isRunning = false;
            if(d != null)
                d.stop();
            if(h != null)
                h.stop();
            System.out.println("Server: stopped");
        }
    }

    public void stopSession(String name){
        if(!d.stopByName(name))
            System.out.println("Error: Wrong session name");
    }
}
