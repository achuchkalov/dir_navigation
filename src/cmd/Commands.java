package cmd;

public enum Commands {
    CD,
    LS,
    DEL,
    MKDIR;
    
    public static Commands get(int index) throws ArrayIndexOutOfBoundsException {
        return Commands.values()[index];
    }
    public int index() {
        return this.ordinal();
    } 
}
