package cmd;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import net.Handler;
import net.Message;
import net.Packet;

public class CmdHandler implements Handler{
    private Message m=null;
    private Path curDir = Paths.get("/Users/sky/testing");



    @Override
    public String init(){
        return usage() + ls();
    }

    @Override
    public Message getMessage(){
        return m;
    }

    @Override
    public String execute(Object a) {
        Packet p = (Packet)a;
        m = p;
        return execute(p.getCmd(), p.getParam());
    }
    public String execute(Commands cmd, String param){
        if(cmd == Commands.LS){
            return ls();
        }else if(cmd == Commands.CD){
            return cd(param);
        } else if(cmd == Commands.MKDIR) {
            return mkdir(param);
        } else if(cmd == Commands.DEL) {
            return del(param);
        } else {
            return usage();
        }
    }

    private String usage(){
        return "Usage: \n" +
                "cd: change directory\n" +
                "ls: list files in current directory\n" +
                "del: delete directory recursively\n" +
                "mkdir: create new directory\n" +
                "exit: exit\n\n";
    }

    private String ls() {
        DirectoryStream<Path> stream = null;
        try{
            stream = Files.newDirectoryStream(curDir);
            String res = "Current directory: " + curDir.toAbsolutePath() + "\n\n";
            for(Path p : stream)
                res += (p.toFile().isDirectory() ? "dir:  " : "file: ") + p.getFileName().toString() + "\n";
            stream.close();
            return "------------------------------\n" +
                    res + "\n" +
                    "------------------------------\n\n";
        }catch(IOException e) {
            try{
                if(stream != null)
                    stream.close();
            } catch(IOException ie) {}
            stringAndLS("Wrong directory!");
            return "";
        }
    }

    private String cd(String param) {
        if(param.contains("..")){
            System.out.println(curDir.toAbsolutePath());
            if (curDir.toAbsolutePath().toString().equals("/Users/sky/testing")){
                return stringAndLS("Access denied");
            }

            curDir = curDir.getParent();
            if (param.equals("..") || param.equals("../")){
                return ls();
            } else {
                return stringAndLS("Going up only per one dir is allowed");
            }
        }

        try{
            Files.newDirectoryStream(curDir.resolve(param)).close();
            curDir = curDir.resolve(param);
        }
        catch(InvalidPathException | IOException e) {
            return stringAndLS("Error: No such dir");
        }
        return execute(Commands.LS, null);
    }

    private String mkdir(String param) {
        try{
            if(Files.exists(curDir.resolve(param)) && Files.isDirectory(curDir.resolve(param))){
                return stringAndLS("Error: Already exists");
            }
            Files.createDirectories(curDir.resolve(param));
            return stringAndLS("Successfully created");
        } catch(FileAlreadyExistsException e) {
            return stringAndLS("Error: Name conflict");
        } catch(AccessDeniedException e) {
            return stringAndLS("Error: Access denied");
        } catch (IOException ex) {
            return stringAndLS("Error: Unknown error");
        }
    }

    private String del(String param){
        if(param.contains("..")){
            return stringAndLS("Param should not contain ..");
        }
        if (!curDir.resolve(param).toAbsolutePath().toString().startsWith("/Users/sky/testing")){
            return stringAndLS("Access denied");
        }
        try{
            if(!Files.exists(curDir.resolve(param))){
                return stringAndLS("Error: No such file or directory");
            }
            removeRecursive(curDir.resolve(param));
            return stringAndLS("Successfully deleted");
        } catch(AccessDeniedException e) {
            return stringAndLS("Error: Access denied");
        } catch (IOException ex) {
            return stringAndLS("Error: Unknown error");
        }
    }

    private String stringAndLS(String str){
        return str + "\n" + ls();
    }

    private void removeRecursive(Path path) throws IOException
    {
        Files.walkFileTree(path, new SimpleFileVisitor<Path>()
        {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                    throws IOException
            {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException
            {
                if (exc == null)
                {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
                throw exc;
            }
        });
    }
}
