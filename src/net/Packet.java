package net;

import java.io.Serializable;

import cmd.Commands;
import net.Message;

public class Packet implements Message, Serializable {
    
    private Commands cmd;
    private String param;
    
    public Packet(Commands type, String str) {
        if (type == null || str == null) {
            throw new IllegalArgumentException("Null arg");
        }
        this.cmd = type;
        if(type != Commands.LS)
            this.param = str;
        else
            this.param = "";
    }
    


    public Commands getCmd() throws NullPointerException {
        if (cmd == null) {
            throw new NullPointerException("Null cmd");
        }
        
        return cmd;
    }
    
    public String getParam() throws NullPointerException {
        if (param == null) {
            throw new NullPointerException("null param");
        }
        
        return param;
    }
    
    @Override
    public String toString() {
        return cmd + " " + param;
    } 
}
