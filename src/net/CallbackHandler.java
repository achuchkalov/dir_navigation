package net;

public interface CallbackHandler {
    public void handle(String state);
}
